# Knex-Sqlite

Little wrapper library for creating an instance of knex for an sqlite3 database.

## Usage

```javascript
import knexSqlite, { OPEN_READONLY } from 'knex-sqlite';

const knex = knexSqlite(
    // Required: the name of your databse
    './my-database.sqlite',
    // Optional: the file open mode (defaults to read-write)
    OPEN_READONLY,
    // Optional: the knex constructor options
    {
        debug: true
    }
);

console.log(await knex.select('*').from('blog_posts'));
```


## Dependencies
 - knex: ^2.0.0
 - sqlite3: ^5.0.5
<a name="module_knex-sqlite"></a>

## knex-sqlite
Knex Sqlite3


* [knex-sqlite](#module_knex-sqlite)
    * _static_
        * [.default](#module_knex-sqlite.default) ⇒ <code>Knex</code>
        * [.OPEN_READONLY](#module_knex-sqlite.OPEN_READONLY) : <code>number</code>
        * [.OPEN_READWRITE](#module_knex-sqlite.OPEN_READWRITE) : <code>number</code>
        * [.OPEN_CREATE](#module_knex-sqlite.OPEN_CREATE) : <code>number</code>
    * _inner_
        * [~mode](#module_knex-sqlite..mode) : <code>OPEN\_READONLY</code> \| <code>OPEN\_READWRITE</code> \| <code>OPEN\_CREATE</code>
        * [~Knex](#external_Knex)

<a name="module_knex-sqlite.default"></a>

### knex-sqlite.default ⇒ <code>Knex</code>
Return an instance of knex for an sqlite3 database.
The filename and mode arguments are the same as those in the sqlite3 library

**Kind**: static property of [<code>knex-sqlite</code>](#module_knex-sqlite)  
**Returns**: <code>Knex</code> - Instance of knex library  
**See**: [node-sqlite3](https://github.com/mapbox/node-sqlite3/wiki/API#new-sqlite3databasefilename-mode-callback)  

| Param | Type | Description |
| --- | --- | --- |
| filename | <code>string</code> | The database to access |
| mode | <code>mode</code> | The mode to open the database as |
| options | <code>object</code> | Additional knex options |

<a name="module_knex-sqlite.OPEN_READONLY"></a>

### knex-sqlite.OPEN\_READONLY : <code>number</code>
Open file as read only

**Kind**: static typedef of [<code>knex-sqlite</code>](#module_knex-sqlite)  
<a name="module_knex-sqlite.OPEN_READWRITE"></a>

### knex-sqlite.OPEN\_READWRITE : <code>number</code>
Open file as read-write

**Kind**: static typedef of [<code>knex-sqlite</code>](#module_knex-sqlite)  
<a name="module_knex-sqlite.OPEN_CREATE"></a>

### knex-sqlite.OPEN\_CREATE : <code>number</code>
Create the file

**Kind**: static typedef of [<code>knex-sqlite</code>](#module_knex-sqlite)  
<a name="module_knex-sqlite..mode"></a>

### knex-sqlite~mode : <code>OPEN\_READONLY</code> \| <code>OPEN\_READWRITE</code> \| <code>OPEN\_CREATE</code>
Modes for opening the database with

**Kind**: inner typedef of [<code>knex-sqlite</code>](#module_knex-sqlite)  
**See**: [node-sqlite3](https://github.com/mapbox/node-sqlite3/wiki/API#new-sqlite3databasefilename-mode-callback)  
<a name="external_Knex"></a>

### knex-sqlite~Knex
Knex instance

**Kind**: inner external of [<code>knex-sqlite</code>](#module_knex-sqlite)  
**See**: [Knex](https://knexjs.org/#Builder)  
