# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.1](https://gitlab.com/paulkiddle/knex-sqlite/compare/v2.0.0...v2.0.1) (2022-04-27)

* Docs: Add usage example

## [2.0.0](https://gitlab.com/paulkiddle/knex-sqlite/compare/v1.0.0...v2.0.0) (2022-04-27)


### ⚠ BREAKING CHANGES

* Upgrade to [knex 2.x](https://github.com/knex/knex/releases/tag/2.0.0)

* Upgrade to [knex 2.x](https://github.com/knex/knex/releases/tag/2.0.0) ([ca05a57](https://gitlab.com/paulkiddle/knex-sqlite/commit/ca05a57906a59a8e51486c054cd91727365e201d))
