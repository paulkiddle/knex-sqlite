/**
 * Knex Sqlite3
 * @module knex-sqlite
 */

/**
 * Knex instance
 * @external Knex
 * @see {@link https://knexjs.org/#Builder|Knex}
 */
import knex from 'knex';

import sqlite3 from 'sqlite3';

/**
 * Modes for opening the database with
 * @see {@link https://github.com/mapbox/node-sqlite3/wiki/API#new-sqlite3databasefilename-mode-callback|node-sqlite3}
 * @typedef {OPEN_READONLY|OPEN_READWRITE|OPEN_CREATE} mode
 */

export const {
	/**
	 * Open file as read only
	 * @typedef {number} module:knex-sqlite.OPEN_READONLY
	 **/
	OPEN_READONLY,
	/**
	 * Open file as read-write
	 * @typedef {number} module:knex-sqlite.OPEN_READWRITE
	 **/
	OPEN_READWRITE,
	/**
	 * Create the file
	 * @typedef {number} module:knex-sqlite.OPEN_CREATE
	 **/
	OPEN_CREATE
} = sqlite3;

/**
 * Return an instance of knex for an sqlite3 database.
 * The filename and mode arguments are the same as those in the sqlite3 library
 * @see {@link https://github.com/mapbox/node-sqlite3/wiki/API#new-sqlite3databasefilename-mode-callback|node-sqlite3}
 * @param	{string} filename The database to access
 * @param	{mode} mode The mode to open the database as
 * @param {object} options Additional knex options
 * @returns {Knex} Instance of knex library
 * @name module:knex-sqlite.default
 */
export default function getDatabase(filename, mode, options = {}){
	const connection = { filename, mode };

	return knex({
		connection,
		...options,
		client: 'sqlite3'
	});
}
