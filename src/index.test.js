import lib from './index.js';

test('Returns knex instance', ()=>{
	expect(lib()).toBeInstanceOf(Function);
	expect(Object.keys(lib())).toMatchSnapshot();
});
