const semver = require('semver');
const package = require('../package.json');
const version = semver.coerce(process.version).toString();
const { strict: assert } = require('assert');
const spec = package.engines.node;
const minVersions = semver.toComparators(spec).map(c=>semver.minVersion(c[0]).toString());


if(process.argv[2] === 'ls') {
	for(const v of minVersions) {
		console.log(v);
	}
} else {
	assert(
		minVersions.includes(version),
		`Wrong version of node:
			You should use node version ${minVersions.join(' or ')}, the minimum that satisfies the spec in package.json (${spec}).
			You are using ${version}.`
	);
}